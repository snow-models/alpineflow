#ifndef __WORKERCLASS_H__
#define __WORKERCLASS_H__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "time.h"

#include "datinClass.h"
#include "ioClass.h"
#include "catchmentClass.h"
#include "errorClass.h"

using namespace std;

class workerClass
{
	public:
	workerClass(const datinClass&, const ioClass&);

	void runSimulation();
	
	private:
	bool startPrint;
	datinClass datin;
	ioClass io;
};

#endif
