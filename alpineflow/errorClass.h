#ifndef __ERRORCLASS_H__
#define __ERRORCLASS_H__

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include "datinClass.h"
#include "catchmentClass.h"

using namespace std;

class errorClass
{
	public:
	errorClass(const datinClass&, const catchmentClass&);
	void shrinkComputed();
	void getFilt();
	void getNashIndex();
	void getr2Index();

	vector<double> compFlow, compTemp;
	vector<double> compFlowFilt, compTempFilt;
	vector<double> measFlowFilt, measTempFilt;

	double nashIndexFlow, nashIndexTemp;
	double r2IndexFlow, r2IndexTemp;
	int offset;	
	int nErrSteps;

	private:
	datinClass datin;
	catchmentClass catchment;
	void getMeans();
	
	vector<double> compFlowTotal, compTempTotal;	
	double meanCompFlow, meanMeasFlow, meanCompTemp, meanMeasTemp;
};

#endif
