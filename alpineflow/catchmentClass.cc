#include "catchmentClass.h"

catchmentClass::catchmentClass(const datinClass& inDatin) : datin(inDatin)
{
}

void catchmentClass::getParameters()
{
	 if (datin.montecarloFlag) {
                recharge = (rand()%100)/100.*(datin.recharge_sup-datin.recharge_inf) + datin.recharge_inf;
                tauUpper = (rand()%100)/100.*(datin.tauUpper_sup-datin.tauUpper_inf) + datin.tauUpper_inf;
                tauLower = (rand()%100)/100.*(datin.tauLower_sup-datin.tauLower_inf) + datin.tauLower_inf;

                kappaSoil = (rand()%100)/100.*(datin.kappaSoil_sup-datin.kappaSoil_inf) + datin.kappaSoil_inf;

                recharge = recharge/(1000.*3600.*24.);
                tauUpper = tauUpper*(24.*3600.);
                tauLower = tauLower*(24.*3600.);
		kappaSoil = kappaSoil*(24.*3600.);
        } else {
                recharge = datin.recharge;
                tauUpper = datin.tauUpper;
                tauLower = datin.tauLower;

                kappaSoil = datin.kappaSoil;

                recharge = recharge/(1000.*3600.*24.);
                tauUpper = tauUpper*(24.*3600.);
                tauLower = tauLower*(24.*3600.);
		kappaSoil = kappaSoil*(24.*3600.);
        }
}
void catchmentClass::initialize()
{
	const int nSubc = datin.nSubc;
	const int nSteps = datin.nSteps;
	const int nStreamPixel = datin.nStreamPixel;

        cout << "ALLOCATING VECTORS (SIZE = " << nSubc << ")" <<  endl;
        length.resize(nSubc);
        deltaElevation.resize(nSubc);
        area.resize(nSubc);
        drainageArea.resize(nSubc);
	drainageLength.resize(nSubc);
        infiltUpper.resize(nSubc);
        infiltLower.resize(nSubc);
        upperVol.resize(nSubc);
        lowerVol.resize(nSubc);
        upperFlow.resize(nSubc);
        lowerFlow.resize(nSubc);
        inStreamFlow.resize(nSubc);
        outStreamFlow.resize(nSubc);
        headArea.resize(nSubc);
        outletFlow.resize(nSteps);
        pixelFlow.resize(nStreamPixel);

        infiltrationTemp.resize(nSubc);
        upperTemp.resize(nSubc);
        lowerTemp.resize(nSubc);
        soilTempMean.resize(nSubc);
        inStreamTemp.resize(nSubc);
        outStreamTemp.resize(nSubc);
        streamTemp.resize(nSubc);
        outletTemp.resize(nSteps);
	pixelTemp.resize(nStreamPixel);

        cout << "COMPUTING SUB-CATCHMENT AREAS" << endl;
        for (unsigned int iy=0; iy<datin.nRowsDem; iy++) {
                for (unsigned int ix=0; ix<datin.nColsDem; ix++) {
                        if (datin.subcatchDTM[iy][ix] >= 0.) {    //check if subcatchDTM is a double or a integer array
                                const int p = datin.subcatchDTM[iy][ix];
                                area[p]+=datin.cellSizeDem*datin.cellSizeDem;
                        }
                }
        }
	
	areaTotal = 0.;
        cout << "COMPUTING STREAM LENGTHS AND ELEVATIONS" << endl;
        for (unsigned int i=0; i<nSubc; i++) {
                const int p1 = datin.firstStreamPixel[i];
                const int p2 = datin.lastStreamPixel[i];
                length[i] = datin.pixelLength[p1]-datin.pixelLength[p2];
                deltaElevation[i] = datin.pixelCoordZ[p1]-datin.pixelCoordZ[p2];
                areaTotal+=area[i];
        }

	cout << "COMPUTING DRAINAGE DENSITIES" << endl;
	for (unsigned int i=0; i<nSubc; i++) {
                if (datin.hortonOrder[i] == 1) {
                        drainageArea[i] = area[i];
                        drainageLength[i] = length[i];
                        headArea[i] = datin.pixelArea[datin.firstStreamPixel[i]];
                } else {
                        const int p1 = datin.previousStream[i][0];
                        const int p2 = datin.previousStream[i][1];
                        drainageArea[i] = drainageArea[p1]+drainageArea[p2]+area[i];
                        drainageLength[i] = drainageLength[p1]+drainageLength[p2]+length[i];
                        headArea[i] = 0.;
                }
        }
	
	if (datin.tempFlag) {	
        	cout << "COMPUTING SOIL TEMPERATURE IN LOWER COMPARTMENT" << endl;
        	for (unsigned int i=0; i<nSubc; i++) {
                	for (unsigned int j=0; j<nSteps; j++) {
                        	soilTempMean[i] += datin.soilTemp[j][i]/nSteps;
                	}
        	}
	}

        for (unsigned int i=0; i<nSubc; i++) {
                cout << "	subc " << i;
                cout << ": length = " << length[i];
                cout << "; area = " << area[i];
		cout << "; drainage density = " << drainageLength[i]/(sqrt(4.*drainageArea[i]/3.1416)); 
                cout  << endl;
        }
}

void catchmentClass::setInitCond()
{
	for (unsigned int i=0; i<datin.nSubc; i++) {
                upperVol[i] = 0.0*area[i];
                lowerVol[i] = 0.3*area[i];

                upperTemp[i] = 273.15;
                lowerTemp[i] = 278.15;
                inStreamTemp[i] = 273.15;
                outStreamTemp[i] = 273.15;
        }
}

void catchmentClass::compMassBalance(const int& step)
{
        for (unsigned int i=0; i<datin.nSubc; i++) {
                infiltLower[i] = min(datin.infiltration[step-1][i]*0.85, recharge)*area[i];
		infiltUpper[i] = datin.infiltration[step-1][i]*0.85*area[i] - infiltLower[i];
                const double tauUpper_i = tauUpper*pow(area[i]/areaTotal,0.3);
                const double tauLower_i = tauLower*pow(area[i]/areaTotal,0.3);

                lowerFlow[i] = lowerVol[i]/tauLower_i;                                                                         // Flux [m^3/s]
                upperFlow[i] = upperVol[i]/tauUpper_i;                                                                         // Flux [m^3/s]
		if (datin.hortonOrder[i] == 1) {
			outStreamFlow[i] = lowerFlow[i]+upperFlow[i];
			inStreamFlow[i] = outStreamFlow[i] * headArea[i]/area[i];
		}
		else {
			const int p1 = datin.previousStream[i][0];
			const int p2 = datin.previousStream[i][1];
			inStreamFlow[i] = outStreamFlow[p1] + outStreamFlow[p2];
			outStreamFlow[i] = inStreamFlow[i] + lowerFlow[i]+upperFlow[i];
		}

		upperVol[i] = max(0., upperVol[i] + datin.deltaT*(infiltUpper[i] - upperFlow[i]));
                lowerVol[i] = max(0., lowerVol[i] + datin.deltaT*(infiltLower[i] - lowerFlow[i]));
	}	
	outletFlow[step-1] = outStreamFlow[datin.outlet];
}

void catchmentClass::compEnergyBalance(const int& step)
{
	for (unsigned int i=0; i<datin.nSubc; i++) {

                infiltrationTemp[i] = datin.soilTemp[step-1][i];

                if (upperVol[i]==0) {
                        upperTemp[i] = 273.15;
                } else {
                        upperTemp[i] = upperTemp[i] + datin.deltaT/upperVol[i]*infiltUpper[i]*(infiltrationTemp[i]-upperTemp[i]) + datin.deltaT*(datin.soilTemp[step-1][i]-upperTemp[i])/kappaSoil;
                        upperTemp[i] = max(upperTemp[i], 273.15);
                }
                if (lowerVol[i]==0) {
                        lowerTemp[i] = 273.15;
                } else {
                        lowerTemp[i] = lowerTemp[i] + datin.deltaT/lowerVol[i]*infiltLower[i]*(infiltrationTemp[i]-lowerTemp[i]) + datin.deltaT*(soilTempMean[i]-lowerTemp[i])/kappaSoil;
                        lowerTemp[i] = max(lowerTemp[i], 273.15);
                }
                const double lateralTemp = (upperFlow[i]*upperTemp[i] + lowerFlow[i]*lowerTemp[i] )/(upperFlow[i]+lowerFlow[i]);

                if (datin.hortonOrder[i]==1) {
                        inStreamTemp[i] = lateralTemp;
                } else {
                        const int p1 = datin.previousStream[i][0];
                        const int p2 = datin.previousStream[i][1];
                        inStreamTemp[i] = (outStreamTemp[p1]*outStreamFlow[p1]+outStreamTemp[p2]*outStreamFlow[p2])/(outStreamFlow[p1]+outStreamFlow[p2]);
                }
                streamTemp[i] = (inStreamTemp[i]+outStreamTemp[i])/2.;

		const double rho_w = 1000.;                  // kg/m3
	        const double rho_a = 1.30;                           // kg/m3
       		const double cp_w = 4190.;                   // J/(kg K)
	        const double cp_a = 1010.;                           // J/(kg K)
	        const double latentHeat = 2260000.;                  // J/kg
	        const double stefanBolt = 5.67E-8;
	        const double emissivity = 0.995;
                const double emittedRad = emissivity*stefanBolt*pow(streamTemp[i],4);
                const double gravity = 9.81;
                const double albedo = 0.1;
                const double p_a = 101325;
                const double soilConductivity = 0.004;
                const double streamWidth = 3.0*pow(0.5*(inStreamFlow[i]+outStreamFlow[i]), 0.49);
		const double deltaZ = 1.;

                const double inletFlux = rho_w*cp_w*inStreamFlow[i]*inStreamTemp[i];
                const double lateralFlux = rho_w*cp_w*(outStreamFlow[i]-inStreamFlow[i])*lateralTemp;
                const double sensibleFlux = rho_a*cp_a*datin.bulkCoeff[step-1][i]*datin.windSpeed[step-1][i]*(datin.airTemp[step-1][i]-streamTemp[i])*streamWidth*length[i];
                const double latentFlux = (rho_a*0.622*latentHeat/p_a)*datin.bulkCoeff[step-1][i]*datin.windSpeed[step-1][i]*(datin.relativeHumidity[step-1][i]*compVaporPressure(datin.airTemp[step-1][i]) - compVaporPressure(streamTemp[i]))*streamWidth*length[i];
                const double conductiveFlux = soilConductivity*(datin.soilTemp[step-1][i]-streamTemp[i])/deltaZ*streamWidth*length[i];
                const double radiativeFlux = ((1.-albedo)*datin.shortRad[step-1][i]+datin.longRad[step-1][i]-emittedRad)*streamWidth*length[i];
                const double frictionFlux = deltaElevation[i]*gravity*rho_w*(inStreamFlow[i]+outStreamFlow[i])/2.;

               	outStreamTemp[i] = max(273.15, 1./(rho_w*cp_w*outStreamFlow[i]) * (inletFlux+lateralFlux+sensibleFlux+latentFlux+radiativeFlux+conductiveFlux+frictionFlux));
//		outStreamTemp[i] = max(273.15, 1./(rho_w*cp_w*outStreamFlow[i]) * (inletFlux+lateralFlux));         // Neglect in-stream energy exchanges
        }
        outletTemp[step-1] = outStreamTemp[datin.outlet];
//	outletTemp[step-1] = (upperTemp[datin.outlet]*upperFlow[datin.outlet]+lowerTemp[datin.outlet]*lowerFlow[datin.outlet])/(upperFlow[datin.outlet]+lowerFlow[datin.outlet]);
}

void catchmentClass::compNetwork()
{
	for (unsigned int i=0; i<datin.nSubc; i++) {
                const int j1 = datin.firstStreamPixel[i];
                const int j2 = datin.lastStreamPixel[i];
                const int nPixel = j2-j1+1;
                const double deltaFlow = (outStreamFlow[i]-inStreamFlow[i])/(nPixel);
		const double deltaArea = (area[i]-headArea[i])/nPixel;
		const double deltaTemp = (outStreamTemp[i]-inStreamTemp[i])/(nPixel);
                for (unsigned int j=0; j<nPixel; j++) {
                        pixelFlow[j1+j] = (inStreamFlow[i]+j*deltaFlow)/(drainageArea[i]-area[i]+headArea[i]+(j+1)*deltaArea) * 1000. * 3600.;		// plot specific streamflow in mm/h
                        pixelTemp[j1+j] = inStreamTemp[i]+j*deltaTemp;
                }
        }
}

double catchmentClass::compVaporPressure(const double& temp)
{
        // see Brutsaert p. 28 ---- temp in K
        const double a0 = 6984.505294;
        const double a1 = -188.9039310;
        const double a2 = 2.133357675;
        const double a3 = -1.288580973 * pow(10,-2);
        const double a4 = 4.393587233 * pow(10,-5);
        const double a5 = -8.023923082 * pow(10,-8);
        const double a6 = 6.136820929 * pow(10,-11);

        double vaporPressure;
        vaporPressure = a5 + temp*a6;
        vaporPressure = a4 + temp*vaporPressure;
        vaporPressure = a3 + temp*vaporPressure;
        vaporPressure = a2 + temp*vaporPressure;
        vaporPressure = a1 + temp*vaporPressure;
        vaporPressure = a0 + temp*vaporPressure;

        vaporPressure *= 0.01;          // from HPa to Pa

        return vaporPressure;
}

