#include "datinClass.h"

datinClass::datinClass(const ioClass& inIo) : io(inIo)
{
}

void datinClass::getParameters()
{
	ifstream fileParameters;
	fileParameters.open(io.fileParametersName.c_str());

	cout << "READING FILE OF PARAMETERS" << endl;
        std::string line;
	getline(fileParameters,line);
        stringstream(line) >> deltaT;
        getline(fileParameters,line);
	stringstream(line) >> recharge >> recharge_inf >> recharge_sup;
	getline(fileParameters, line);
        stringstream(line) >> tauUpper >> tauUpper_inf >> tauUpper_sup;
        getline(fileParameters, line);
        stringstream(line) >> tauLower >> tauLower_inf >> tauLower_sup;
	getline(fileParameters, line);
	stringstream(line) >> tempFlag;
	if (tempFlag) {
		getline(fileParameters, line);
        	stringstream(line) >> kappaSoil >> kappaSoil_inf >> kappaSoil_sup;
     	} else {
		getline(fileParameters, line);
	}
	getline(fileParameters, line);	
	stringstream(line) >> montecarloFlag >> nSimulations;
	getline(fileParameters,line);
	stringstream(line) >> outputFlag;
	getline(fileParameters, line);
        stringstream(line) >> yDistIni >> mDistIni >> dDistIni >> hDistIni >> nPrint;
	getline(fileParameters, line);
	stringstream(line) >> errorFlag >> indexType;
	getline(fileParameters, line);
	stringstream(line) >> yErrIni >> mErrIni >> dErrIni >> hErrIni;
	getline(fileParameters, line);
	stringstream(line) >> yErrEnd >> mErrEnd >> dErrEnd >> hErrEnd;
	getline(fileParameters, line);
	stringstream(line) >> outlet;
        fileParameters.close();

	if (! montecarloFlag) {
		cout << "	maximum racherge rate = " << recharge << " mm/d" << endl;
        	cout << "	tau upper = " << tauUpper << " d" << endl;
        	cout << "	tau lower = " << tauLower << " d" << endl;
		if (tempFlag) {
			cout << "	Energy exchange coefficient water-soil = " << kappaSoil << " d" << endl;
		}
	} else {
		cout << "	Flag for running a montecarlo simulation ON; number of simulations = " << nSimulations << endl;
	}
	if (outputFlag) {
		cout << "	Flag for extensive output ON" << endl;
		cout << "	Start date for exstensive printing: " << yDistIni << "/" << mDistIni << "/" << dDistIni << " " << hDistIni << ":00" << endl;
                cout << "	Number of timestep for printing: " << nPrint << endl;
	}
	if (errorFlag) {
		cout << "	Flag for computing error ON" << endl;
		cout << "	Start date for error calculations: " << yErrIni << "/" << mErrIni << "/" << dErrIni << " " << hErrIni << ":00" << endl;
		cout << "	End date for error calculations: " << yErrEnd << "/" << mErrEnd << "/" << dErrEnd << " " << hErrEnd << ":00" << endl;
	}
	cout << "	Results will be printed for the outlet section: " << outlet << endl;
}

void datinClass::getNetwork()
{
	ifstream fileTree;
	fileTree.open(io.fileTreeName.c_str());

	cout << "READING FILE TREE" << endl;
        string line;
	int streamIndex;
	fileTree >> nSubc;
	nSubc++;
	firstStreamPixel.resize(nSubc);
	lastStreamPixel.resize(nSubc);
	previousStream.resize( nSubc, vector<int> (2));
	nextStream.resize(nSubc);
	hortonOrder.resize(nSubc);
	getline(fileTree,line);
        stringstream(line) >> firstStreamPixel[nSubc-1] >> lastStreamPixel[nSubc-1] >> nextStream[nSubc-1] >> previousStream[nSubc-1][0] >> previousStream[nSubc-1][1] >> hortonOrder[nSubc-1];
        for (unsigned int i=1; i<nSubc; i++) {
                getline(fileTree,line);
                stringstream(line) >> streamIndex >> firstStreamPixel[nSubc-i-1] >> lastStreamPixel[nSubc-i-1] >> nextStream[nSubc-i-1] >> previousStream[nSubc-i-1][0] >> previousStream[nSubc-i-1][1] >> hortonOrder[nSubc-i-1];
	}
        fileTree.close();
}

void datinClass::getWatershed()
{
	ifstream fileW;
        fileW.open(io.fileWName.c_str());

        cout << "READING FILE OF SUBCATCHMENTS" << endl;
        string label;
        fileW >> label >> nColsDem;
        fileW >> label >> nRowsDem;
        fileW >> label >> xllDem;
        fileW >> label >> yllDem;
        fileW >> label >> cellSizeDem;
        fileW >> label >> nodata;
        subcatchDTM.resize (nRowsDem, std::vector<double>(nColsDem));
        for (unsigned int iy=0; iy<nRowsDem; iy++) {
                for (unsigned int ix=0; ix<nColsDem; ix++) {
                        fileW >> subcatchDTM[iy][ix];
                }
        }
	fileW.close();
}

void datinClass::getCoordinates()
{
	ifstream fileCoord;
        cout << "READING FILE COORD" << endl;
        fileCoord.open(io.fileCoordName.c_str());
        nStreamPixel = 0;
	string line;
        while (!fileCoord.eof()) {
                getline(fileCoord,line);
                if (!line.empty()) {
                        nStreamPixel++;
                        pixelLength.resize(nStreamPixel);
			pixelCoordX.resize(nStreamPixel);
			pixelCoordY.resize(nStreamPixel);
			pixelCoordZ.resize(nStreamPixel);
			pixelArea.resize(nStreamPixel);
                        stringstream(line) >> pixelCoordX[nStreamPixel-1] >> pixelCoordY[nStreamPixel-1] >> pixelLength[nStreamPixel-1] >> pixelCoordZ[nStreamPixel-1] >> pixelArea[nStreamPixel-1];
	        }
        }
        fileCoord.close();
}
	
void datinClass::getMeteoForcings()
{
	ifstream fileInfiltration;
	cout << "READING FILE FOR STREAMFLOW SIMULATION" << endl;
	fileInfiltration.open(io.fileInfiltrationName.c_str());
	nSteps = 0;
	string line;
	getline(fileInfiltration, line);
	while (!fileInfiltration.eof()) {
		fileInfiltration >> line;
		if (!line.empty()) {
			nSteps++;
			timeStamp.resize(nSteps);
			infiltration.resize(nSteps, vector<double> (nSubc) );
			timeStamp[nSteps-1] = line;
			for (unsigned int i=0; i<nSubc; i++) {
				fileInfiltration >> infiltration[nSteps-1][i];
				infiltration[nSteps-1][i] *= 1./(1000.*3600.);
				if (nSteps == 1) {
					infiltration[nSteps-1][i] = 0;
				}
			}
		}
	} 
        fileInfiltration.close();
	cout << "	Simulation starts: " << timeStamp[0] << " and ends: " << timeStamp[nSteps-1] << endl;
	cout << "NUMBER OF TIME STEPS = " << nSteps << endl;

	if (tempFlag) {
		ifstream fileAirTemp, fileSoilTemp, fileRelativeHumidity, fileShortRad, fileLongRad, fileWindspeed, fileBulkCoeff;
		cout << "READING FILES FOR TEMPERATURE SIMULATION" << endl;
 		airTemp.resize(nSteps, vector<double> (nSubc));
		fileAirTemp.open(io.fileAirTempName.c_str());
		soilTemp.resize(nSteps, vector<double> (nSubc));
                fileSoilTemp.open(io.fileSoilTempName.c_str());
		relativeHumidity.resize(nSteps, vector<double> (nSubc));
                fileRelativeHumidity.open(io.fileRelativeHumidityName.c_str());
		shortRad.resize(nSteps, vector<double> (nSubc));
                fileShortRad.open(io.fileShortRadName.c_str());
		longRad.resize(nSteps, vector<double> (nSubc));
                fileLongRad.open(io.fileLongRadName.c_str());
		windSpeed.resize(nSteps, vector<double> (nSubc));
                fileWindspeed.open(io.fileWindspeedName.c_str());
		bulkCoeff.resize(nSteps, vector<double> (nSubc));
                fileBulkCoeff.open(io.fileBulkCoeffName.c_str());
		string date;
		getline(fileAirTemp, line);
		getline(fileSoilTemp, line);
		getline(fileRelativeHumidity, line);
		getline(fileShortRad, line);
		getline(fileLongRad, line);
		getline(fileWindspeed, line);
		getline(fileBulkCoeff, line);
	        for (unsigned int j=0; j<nSteps; j++) {
			fileAirTemp >> date;
			fileSoilTemp >> date;
			fileRelativeHumidity >> date;
			fileShortRad >> date;
			fileLongRad >> date;
			fileWindspeed >> date;
			fileBulkCoeff >> date;
	                for (unsigned int i=0; i<nSubc; i++) {
	                        fileAirTemp >> airTemp[j][i];
				fileSoilTemp >> soilTemp[j][i];
				fileRelativeHumidity >> relativeHumidity[j][i];
				fileShortRad >> shortRad[j][i];
				fileLongRad >> longRad[j][i];
				fileWindspeed >> windSpeed[j][i];
				fileBulkCoeff >> bulkCoeff[j][i];
	                }
		}
		fileAirTemp.close();
		fileSoilTemp.close();
		fileRelativeHumidity.close();
		fileShortRad.close();
		fileLongRad.close();
		fileWindspeed.close();
		fileBulkCoeff.close();
	}
}

void datinClass::getMeasure()
{
	ifstream fileMeasFlow;
	cout << "RETRIEVING MEASURED STREAMFLOW" << endl;

        nErrSteps = 1;

        fileMeasFlow.open(io.fileMeasFlowName.c_str());

        string yearStr, monthStr, dayStr, hourStr;
        string tempStr;
	double flow, temp;

        getline(fileMeasFlow, tempStr);
        getline(fileMeasFlow, tempStr);
        getline(fileMeasFlow, tempStr);
        int year = 0;
        int month = 0;
        int day = 0;
        int hour = 0;

        while (! ((year == yErrIni) && (month == mErrIni) && (day == dErrIni) && (hour == hErrIni)) ) {
                fileMeasFlow >> tempStr >> flow;
                yearStr = tempStr.substr(5,4);
                monthStr = tempStr.substr(10,2);
                dayStr = tempStr.substr(13,2);
                hourStr = tempStr.substr(16,2);
                stringstream(yearStr) >> year;
                stringstream(monthStr) >> month;
                stringstream(dayStr) >> day;
                stringstream(hourStr) >> hour;
        }
	
	measFlow.resize(nErrSteps);
	measFlow[nErrSteps-1] = flow;
        while (! ((year == yErrEnd) && (month == mErrEnd) && (day == dErrEnd) && (hour == hErrEnd)) ) {
                nErrSteps ++;
                measFlow.resize(nErrSteps);
                fileMeasFlow >> tempStr >> measFlow[nErrSteps-1];
                yearStr = tempStr.substr(5,4);
                monthStr = tempStr.substr(10,2);
                dayStr = tempStr.substr(13,2);
                hourStr = tempStr.substr(16,2);
		stringstream(yearStr) >> year;
                stringstream(monthStr) >> month;
                stringstream(dayStr) >> day;
                stringstream(hourStr) >> hour;
        }
        fileMeasFlow.close();

	if (tempFlag) {
		ifstream fileMeasTemp;
        	cout << "RETRIEVING MEASURED TEMPERATURE" << endl;

        	fileMeasTemp.open(io.fileMeasTempName.c_str());
        	measTemp.resize(nErrSteps);

        	getline(fileMeasTemp, tempStr);
        	getline(fileMeasTemp, tempStr);
        	getline(fileMeasTemp, tempStr);
        	year = 0;
        	month = 0;
        	day = 0;
        	hour = 0;

        	while (! ((year == yErrIni) && (month == mErrIni) && (day == dErrIni) && (hour == hErrIni)) ) {
			fileMeasTemp >> tempStr >> temp;
                	yearStr = tempStr.substr(5,4);
                	monthStr = tempStr.substr(10,2);
                	dayStr = tempStr.substr(13,2);
                	hourStr = tempStr.substr(16,2);
                	stringstream(yearStr) >> year;
                	stringstream(monthStr) >> month;
                	stringstream(dayStr) >> day;
                	stringstream(hourStr) >> hour;
        	}

		measTemp[0] = temp;
        	for (unsigned int i=1; i<nErrSteps; i++) {
                	fileMeasTemp >> tempStr >> measTemp[i];
        	}
        	fileMeasTemp.close();
	}
}
