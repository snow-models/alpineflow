#ifndef __CATCHMENTCLASS_H__
#define __CATCHMENTCLASS_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

#include "datinClass.h"

using namespace std;

class catchmentClass
{
	public:
	catchmentClass(const datinClass&);

	void getParameters();
	void initialize();
	void setInitCond();
	void compMassBalance(const int&);
	void compEnergyBalance(const int&);
	void compNetwork();
	double compVaporPressure(const double&);	

	vector< double > infiltUpper, infiltLower;
	vector< double > upperVol, lowerVol;
	vector< double > upperFlow, lowerFlow;
	vector< double > inStreamFlow, outStreamFlow;
	vector< double > pixelFlow;
	vector< double > outletFlow;

	vector<double> infiltrationTemp;
        vector<double> upperTemp, lowerTemp;
        vector<double> soilTempMean;
        vector<double> inStreamTemp, outStreamTemp, streamTemp;
        vector<double> outletTemp;
        vector<double> pixelTemp;

	vector<double> length;
        vector<double> deltaElevation;
        vector<double> area;
        vector<double> drainageArea, drainageLength;
        vector<double> headArea;
        double areaTotal;
	
	double recharge, tauUpper, tauLower, deltaT;
	double kappaSoil;	

	private:
	datinClass datin;
};

#endif	
