#include "ioClass.h"

void ioClass::readFiles()
{
	cout << "READING FILE OF INPUT/OUPUT" << endl;
	string fileIOName = "../input/fileIO.txt";
	ifstream fileIO;
	fileIO.open(fileIOName.c_str());

	getline(fileIO, fileParametersName);

	getline(fileIO, fileTreeName);
	getline(fileIO, fileCoordName);
	getline(fileIO, fileWName);

	getline(fileIO, fileInfiltrationName);
	getline(fileIO, fileAirTempName);
	getline(fileIO, fileSoilTempName);
	getline(fileIO, fileRelativeHumidityName);
	getline(fileIO, fileShortRadName);
	getline(fileIO, fileLongRadName);
	getline(fileIO, fileWindspeedName);
	getline(fileIO, fileBulkCoeffName);

	getline(fileIO, fileComputedName);
	getline(fileIO, fileMeasFlowName);
	getline(fileIO, fileMeasTempName);
	getline(fileIO, fileIndexName);
	getline(fileIO, fileFiltName);
	getline(fileIO, pathDistributedName);
	
	cout << "	File of parameters: " << fileParametersName << endl;
	cout << "	File of stream network: " << fileTreeName << endl;
	cout << "	File of streams coordinates: " << fileCoordName << endl;
	cout << "	File of subcatchments: " << fileWName << endl;
	cout << "	File of infiltration: " << fileInfiltrationName << endl;
	cout << "	File of air temperature: " << fileAirTempName << endl;
	cout << "	File of soil temperature: " << fileSoilTempName << endl;
	cout << "	File of realtve humidity: " << fileRelativeHumidityName << endl;
	cout << "	File of short wave radiaiton: " << fileShortRadName << endl;
	cout << " 	File of long wave radiation: " << fileLongRadName << endl;
	cout << "	File of wind velocity: " << fileWindspeedName << endl;
	cout << "	File of bulk coefficient: " << fileBulkCoeffName << endl;
	cout << "	File of computed results: " << fileComputedName << endl;
	cout << "	File of measured streamflow: " << fileMeasFlowName << endl;
	cout << "	File of measured temperature: " << fileMeasTempName << endl;
	cout << "	File of Nash index and correlation coefficient: " << fileIndexName << endl;
	cout << "	File of filtered component of streamflow and temperature: " << fileFiltName << endl;
	cout << "	Path of distributed results: " << pathDistributedName << endl;

	fileIO.close();
}
