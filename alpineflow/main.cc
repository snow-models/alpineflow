#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include "ioClass.h"
#include "datinClass.h"
#include "workerClass.h" 

int main()
{	
	ioClass io;
	io.readFiles();

	datinClass datin(io);
	datin.getParameters();
	datin.getNetwork();
	datin.getWatershed();
	datin.getCoordinates();
	datin.getMeteoForcings();
	if (datin.errorFlag) {
		datin.getMeasure();
	}
	workerClass worker(datin, io);
	worker.runSimulation();
	
	return 0;
}
