#ifndef __DATINCLASS_H__
#define __DATINCLASS_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>

#include "ioClass.h"

using namespace std;

class datinClass
{
	public:
	datinClass(const ioClass&);
	void getParameters();
	void getNetwork();
	void getWatershed();
	void getCoordinates();
	void getMeteoForcings();
	void getMeasure();

        double recharge, tauUpper, tauLower, kappaSoil;
	double recharge_sup, tauUpper_sup, tauLower_sup, kappaSoil_sup;
	double recharge_inf, tauUpper_inf, tauLower_inf, kappaSoil_inf;
	bool errorFlag, montecarloFlag, outputFlag, tempFlag;
	int indexType;
	int nColsDem;
        int nRowsDem;
        double xllDem;
        double yllDem;
        double cellSizeDem;
        double nodata;

	int nSubc, nSteps, nStreamPixel, nErrSteps, nSimulations;
	
	double deltaT;
	double yErrIni, mErrIni, dErrIni, hErrIni;
	double yErrEnd, mErrEnd, dErrEnd, hErrEnd;
	double yDistIni, mDistIni, dDistIni, hDistIni, nPrint;
	int outlet;
	
        vector<std::vector<double> > subcatchDTM;

	vector<int> firstStreamPixel;
	vector<int> lastStreamPixel;
	vector< vector<int> > previousStream;
	vector<int> nextStream;
	vector<int> hortonOrder;
	
	vector<double> pixelCoordX, pixelCoordY, pixelCoordZ, pixelLength, pixelArea;

	vector< vector<double> > infiltration;
	vector< vector<double> > airTemp;
	vector< vector<double> > soilTemp;
	vector< vector<double> > shortRad;
	vector< vector<double> > longRad;
	vector< vector<double> > windSpeed;
	vector< vector<double> > relativeHumidity;
	vector< vector<double> > bulkCoeff;

	vector<double> measFlow;
	vector<double> measTemp;

	vector<string> timeStamp;

	private:
	ioClass io;
};

#endif
