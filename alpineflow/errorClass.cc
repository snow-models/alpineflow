#include "errorClass.h"

errorClass::errorClass(const datinClass& inDatin, const catchmentClass& inCatchment) : datin(inDatin), catchment(inCatchment)
{
	compFlowTotal = catchment.outletFlow;
	nErrSteps = datin.measFlow.size();
	if (datin.tempFlag) {
		compTempTotal = catchment.outletTemp;
	}	
}

void errorClass::shrinkComputed()
{
	string yyyyStr, mmStr, ddStr, hhStr;
	int yyyy, mm, dd, hh;
	offset = 0;
        yyyyStr = datin.timeStamp[offset].substr(0,4);
	mmStr = datin.timeStamp[offset].substr(5,2);
	ddStr = datin.timeStamp[offset].substr(8,2);
	hhStr = datin.timeStamp[offset].substr(11,2);
	stringstream(yyyyStr) >> yyyy;
	stringstream(mmStr) >> mm;
	stringstream(ddStr) >> dd;
	stringstream(hhStr) >> hh;
	while (! ((yyyy == datin.yErrIni) && (mm == datin.mErrIni) && (dd == datin.dErrIni) && (hh == datin.hErrIni)) ) {
        	yyyyStr = datin.timeStamp[offset].substr(0,4);
		mmStr = datin.timeStamp[offset].substr(5,2);
		ddStr = datin.timeStamp[offset].substr(8,2);
		hhStr = datin.timeStamp[offset].substr(11,2);
		stringstream(yyyyStr) >> yyyy;
		stringstream(mmStr) >> mm;
		stringstream(ddStr) >> dd;
		stringstream(hhStr) >> hh;

		offset++;
	}
	compFlow.resize(nErrSteps);
	for (unsigned int i=0; i<nErrSteps; i++) {
                compFlow[i] = compFlowTotal[offset+i];
        }
	if (datin.tempFlag) {
		compTemp.resize(nErrSteps);
		for (unsigned int i=0; i<nErrSteps; i++) {
			compTemp[i] = compTempTotal[offset+i]-273.15;
		}
	}
}

void errorClass::getFilt()
{
	compFlowFilt.resize(nErrSteps);
	measFlowFilt.resize(nErrSteps);
	const double delta = 24.;
	int offset = int(delta/2.);
	
	for (unsigned int i=0; i<offset; i++) {
		compFlowFilt[i] = compFlow[i];
		measFlowFilt[i] = datin.measFlow[i];
	}
	for (unsigned int i=offset; i<nErrSteps-offset; i++) {
		compFlowFilt[i] = 0.;
		measFlowFilt[i] = 0.;
		for (int j=-offset; j<offset; j++) {
			compFlowFilt[i] += 1./delta * compFlow[i+j];
			measFlowFilt[i] += 1./delta * datin.measFlow[i+j];
		}
	}
	for (unsigned int i=0; i<offset; i++) {
		compFlowFilt[nErrSteps-offset+i] = compFlow[nErrSteps-offset+i];
		measFlowFilt[nErrSteps-offset+i] = datin.measFlow[nErrSteps-offset+i];
	}

	if (datin.tempFlag) {
		compTempFilt.resize(nErrSteps);
        	measTempFilt.resize(nErrSteps);
        	for (unsigned int i=0; i<offset; i++) {
                	compTempFilt[i] = compTemp[i];
                	measTempFilt[i] = datin.measTemp[i];
        	}
        	for (unsigned int i=offset; i<nErrSteps-offset; i++) {
                	compTempFilt[i] = 0.;
                	measTempFilt[i] = 0.;
                	for (int j=-offset; j<offset; j++) {
                        	compTempFilt[i] += 1./delta * compTemp[i+j];
                        	measTempFilt[i] += 1./delta * datin.measTemp[i+j];
                	}
        	}
        	for (unsigned int i=0; i<offset; i++) {
                	compTempFilt[nErrSteps-offset+i] = compTemp[nErrSteps-offset+i];
                	measTempFilt[nErrSteps-offset+i] = datin.measTemp[nErrSteps-offset+i];
        	}
	}
}

void errorClass::getMeans()
{
	meanMeasFlow = 0.0;
	for (unsigned int i=0; i<nErrSteps; i++) {
		meanMeasFlow = meanMeasFlow + measFlowFilt[i]/nErrSteps;
	}
	if (datin.tempFlag) {
		meanMeasTemp = 0.0;
		for (unsigned int i=0; i<nErrSteps; i++) {
			meanMeasTemp = meanMeasTemp + measTempFilt[i]/nErrSteps;
		}
	}

	meanCompFlow = 0.0;
        for (unsigned int i=0; i<nErrSteps; i++) {
                meanCompFlow = meanCompFlow + compFlowFilt[i]/nErrSteps;
        }
        if (datin.tempFlag) {
                meanCompTemp = 0.0;
                for (unsigned int i=0; i<nErrSteps; i++) {
                        meanCompTemp = meanCompTemp + compTempFilt[i]/nErrSteps;
                }
        }
}

void errorClass::getNashIndex()
{
	getMeans();

	double sum1 = 0.0;
	double sum2 = 0.0;
   	for (unsigned int i=0; i<nErrSteps; i++) {
		const double diff1 = measFlowFilt[i] - compFlowFilt[i];
        	const double diff2 = measFlowFilt[i] - meanMeasFlow;
        	sum1 += diff1 * diff1;
        	sum2 += diff2 * diff2;
    	}
	nashIndexFlow = 1. - sum1/sum2;
	if (datin.tempFlag) {
        	sum1 = 0.0;
        	sum2 = 0.0;
        	for (unsigned int i=0; i<nErrSteps; i++) {
                	const double diff1 = measTempFilt[i] - compTempFilt[i];
                	const double diff2 = measTempFilt[i] - meanMeasTemp;
                	sum1 += diff1 * diff1;
                	sum2 += diff2 * diff2;
		}
        	nashIndexTemp = 1. - sum1/sum2;
	}
}

void errorClass::getr2Index()
{
	getMeans();
	
	double sum1 = 0.0;
	double sum2 = 0.0;
	double sum3 = 0.0;
	for (unsigned int i=0; i<nErrSteps; i++) {
		const double diff1 = measFlowFilt[i] - meanMeasFlow;
		const double diff2 = compFlowFilt[i] - meanCompFlow;
		sum1 += diff1*diff2;
		sum2 += diff1*diff1;
		sum3 += diff2*diff2;
	}
	r2IndexFlow = pow(sum1/(sqrt(sum2)*sqrt(sum3)),2);

	if (datin.tempFlag) {
		sum1 = 0.0;
                sum2 = 0.0;
		sum3 = 0.0;
                for (unsigned int i=0; i<nErrSteps; i++) {
                        const double diff1 = measTempFilt[i] - meanMeasTemp;
			const double diff2 = compTempFilt[i] - meanCompTemp;
			sum1 += diff1*diff2;
	                sum2 += diff1*diff1;
        	        sum3 += diff2*diff2;
       		}
		r2IndexTemp = pow(sum1/(sqrt(sum2)*sqrt(sum3)),2);
	}
}
	

