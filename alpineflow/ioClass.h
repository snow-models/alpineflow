#ifndef __IOCLASS_H__
#define __IOCLASS_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>

using namespace std;

class ioClass
{
	public:
	void readFiles();
	string fileParametersName;

	string fileTreeName;
	string fileCoordName;
	string fileWName;

	string fileInfiltrationName;
	string fileAirTempName;
	string fileSoilTempName;
	string fileRelativeHumidityName;
	string fileShortRadName;
	string fileLongRadName;
	string fileWindspeedName;
	string fileBulkCoeffName;

	string fileComputedName;
	string fileMeasFlowName;
	string fileMeasTempName;
	string fileIndexName;
	string fileFiltName;

	string pathDistributedName;
};

#endif
