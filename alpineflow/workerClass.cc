#include "workerClass.h"
	double printFlowIndex, printTempIndex;

workerClass::workerClass(const datinClass& inDatin, const ioClass& inIo) : datin(inDatin), io(inIo)
{
}
	
void workerClass::runSimulation()
{
        cout << "STARTING THE SIMULATION" << endl;

	ofstream fileComputed;
	ofstream fileFilt, fileIndex;

	string yearStr, monthStr, dayStr, hourStr;
        int year, month, day, hour;
        int iPrint=0;

	double nSimulations;
	if (datin.montecarloFlag) {
		srand(time(NULL));				
		nSimulations = datin.nSimulations;
	} else {
		nSimulations = 1;
	}
	catchmentClass catchment(datin);
	catchment.initialize();			
	for (unsigned int j=0; j<nSimulations; j++) {			
		cout << "	simulation " << j << endl;
		catchment.getParameters();
		catchment.setInitCond();
		int step = 0;
		for (int i=0; i<datin.nSteps; i++) {
			step++;

              		catchment.compMassBalance(step);
			if (datin.tempFlag) {
              			catchment.compEnergyBalance(step);
			}
			yearStr = datin.timeStamp[step-1].substr(0,4);
                	monthStr = datin.timeStamp[step-1].substr(5,2);
                	dayStr = datin.timeStamp[step-1].substr(8,2);
                	hourStr = datin.timeStamp[step-1].substr(11,2);

                	stringstream(yearStr) >> year;
                	stringstream(monthStr) >> month;
                	stringstream(dayStr) >> day;
                	stringstream(hourStr) >> hour;

			if (datin.outputFlag && !datin.montecarloFlag) {
                        	if ((year == datin.yDistIni) && (month == datin.mDistIni) && (day == datin.dDistIni) && (hour == datin.hDistIni)) {
					startPrint = true;
				}
			}
			if (startPrint && iPrint < datin.nPrint) {
                                catchment.compNetwork();
                                string fileDistributedName = io.pathDistributedName;
                                fileDistributedName.append(datin.timeStamp[step-1]);
                                fileDistributedName.append(".txt");
                                ofstream fileDistributed;
                                fileDistributed.open(fileDistributedName.c_str());
                                for (unsigned int k=0; k<datin.nStreamPixel; k++) {
                                          fileDistributed << datin.pixelCoordX[k] << "\t" << datin.pixelCoordY[k] << "\t" << catchment.pixelFlow[k] << "\t" <<  catchment.pixelTemp[k]-273.15 << endl;
                                }
                                fileDistributed.close();
                                iPrint++;
			}
		}
		fileComputed.open(io.fileComputedName.c_str());
		fileComputed << "Drainage area = " << catchment.drainageArea[datin.outlet] << endl;
		if (datin.tempFlag) {
	        	fileComputed << "Time step" << "    " << "Streamflow [mm/h]" << "    " <<  "Temperature [K]" << endl;
			for (unsigned int i=0; i<datin.nSteps; i++) {
                	        fileComputed << datin.timeStamp[i] << "\t" << setprecision(5) << scientific << catchment.outletFlow[i]/catchment.drainageArea[datin.outlet]*1000.*3600. << "\t" << catchment.outletTemp[i]-274.15 <<  endl;
			}
		} else {
			fileComputed << "Time step" << "    " << "Streamflow [mm/h]" << endl;
			for (unsigned int i=0; i<datin.nSteps; i++) {
                                fileComputed << datin.timeStamp[i] << "\t" << setprecision(5) << scientific << catchment.outletFlow[i]/catchment.drainageArea[datin.outlet]*1000.*3600. << endl;
			}
		}

        	fileComputed.close();
		if (datin.montecarloFlag) {
			io.fileFiltName = "../monteCarlo/simulations/output_";
        		io.fileIndexName = "../monteCarlo/simulations/output_";
        		stringstream numberToString;                        
        		string number;                                      
        		numberToString << j;                                
        		number = numberToString.str();                      
        		io.fileFiltName.append(number);                        
        		io.fileIndexName.append(number);                       
        		io.fileFiltName.append("/filtered.txt");              
        		io.fileIndexName.append("/index.txt");                 
		}
        		
		if (datin.errorFlag) {
			errorClass error(datin, catchment);
               		error.shrinkComputed();
               		error.getFilt();
			if (datin.indexType==0) {
               			error.getNashIndex();
			} else if (datin.indexType==1) {
				error.getr2Index();
			}
               		fileFilt.open(io.fileFiltName.c_str());
			if (datin.tempFlag) {
               			fileFilt << "Time step" << "\t" << "Computed flow" << "\t" << "Measured flow" << "\t" << "Computed temperature" << "\t" << "Measured temperature" << endl;
               			for (unsigned int i=0; i<error.nErrSteps; i++) {
                       			fileFilt << datin.timeStamp[error.offset+i] << "\t"  << setprecision(3) << scientific << error.compFlowFilt[i] << "\t" << error.measFlowFilt[i]
					 << "\t" << error.compTempFilt[i] << "\t" << error.measTempFilt[i] << endl;
               			}
			} else {
				fileFilt << "Time step" << "\t" << "Computed flow" << "\t" << "Measured flow" << endl;
				for (unsigned int i=0; i<error.nErrSteps; i++) {
                                        fileFilt << datin.timeStamp[error.offset+i] << "\t"  << setprecision(3) << scientific << error.compFlowFilt[i] << "\t" << error.measFlowFilt[i] << endl;
                                }
			}
               		fileFilt.close();
               		fileIndex.open(io.fileIndexName.c_str());
			fileIndex << "recharge: " << catchment.recharge *1000.*3600.*24. << " [mm/d]" << endl;
			fileIndex << "tauUpper: " << catchment.tauUpper /(3600.*24.) << " [d]" << endl;
			fileIndex << "tauLower: " << catchment.tauLower /(3600.*24.) << " [d]" << endl;
			if (datin.tempFlag) {
                        	fileIndex << "kappaSoil: " << catchment.kappaSoil/(3600.*24.) << " [d]" << endl;
				if (datin.indexType==0) {
					fileIndex << "NS index for flow" << "\t" << "NS index for temperature" << endl;
					fileIndex << error.nashIndexFlow << "\t" << error.nashIndexTemp << endl;
					cout << "NS-flow: " << error.nashIndexFlow << "\t" << "NS-temp: " << error.nashIndexTemp << endl;
				} else if (datin.indexType==1) {
					fileIndex << "r2 index for flow" << "\t" << "r2 index for temperature" << endl;
					fileIndex << error.r2IndexFlow << "\t" << error.r2IndexTemp << endl;
					cout << "r2-flow: " << error.r2IndexFlow << "\t" << "r2-temp: " << error.r2IndexTemp << endl;
				}
			} else {
				if (datin.indexType==0) {
                                        fileIndex << "NS index for flow" << endl;
					fileIndex << error.nashIndexFlow << endl;
					cout << "NS-flow: " << error.nashIndexFlow << endl;
                                } else if (datin.indexType==1) {
                                        fileIndex << "r2 index for flow" << endl;
					fileIndex << error.r2IndexFlow << endl;
					cout << "r2-flow: " << error.r2IndexFlow << endl;
				}
                        }                       
               		fileIndex.close();
		}
	}
}
