# AlpineFlow
This model has been further developed in the new [StreamFlow](https://code.wsl.ch/snow-hydrology/streamflow) model, it is now only kept for reference as all users are urged to use [StreamFlow](https://code.wsl.ch/snow-hydrology/streamflow) instead.

See Michel, A., Schaefli, B., Wever, N., Zekollari, H., Lehning, M., and Huwald, H.: <i>Future water temperature of rivers in Switzerland under climate change investigated with physics-based models</i>, Hydrol. Earth Syst. Sci., 26, 1063–1087, [hess-26-1063-2022](https://doi.org/10.5194/hess-26-1063-2022), 2022.
